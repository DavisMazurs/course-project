import React, { Component } from "react";
import {ButtonProps, Nav} from "react-bootstrap";
import { withRouter } from "react-router";
import { Link, NavLink } from "react-router-dom";
import { IconType } from "react-icons";
import Ikona from "./Ikona";
import { BiUserPlus } from "react-icons/bi";
import { BsFillBarChartFill } from "react-icons/bs";
import { GiMoneyStack } from "react-icons/gi";

const Sidebar: React.FC = () => {
        return (
            <div className="sidebar" data-color="dark">
                <div className="sidebar-wrapper">
                <div className="logo">
                    <Link to='/' className="simple-text">
                    Course Project
                    </Link>
                </div>
                <ul className="nav">
                    <li className="nav-item">
                    <NavLink className="nav-link" to='/'>
                        <Ikona Icon={BsFillBarChartFill} />
                        &nbsp;
                        &nbsp;
                        <p>Dashboard</p>
                    </NavLink>
                    </li>
                    <li className="nav-item">
                    <NavLink className="nav-link" to='/budget'>
                        <Ikona Icon={GiMoneyStack} />
                        &nbsp;
                        &nbsp;
                        <p>Budget App</p>
                    </NavLink>
                    </li>

                </ul>
                </div>
            </div>
        )
}

// const Side: React.FC = () => {

//     return (
//         <>
//             <Nav className="col-md-12 d-none d-md-block bg-light sidebar"
//             activeKey="/home"
//             onSelect={selectedKey => alert(`selected ${selectedKey}`)}
//             >
//             <div className="sidebar-sticky"></div>
//             <Nav.Item>
//                 <Nav.Link href="/home">Active</Nav.Link>
//             </Nav.Item>
//             <Nav.Item>
//                 <Nav.Link eventKey="link-1">Link</Nav.Link>
//             </Nav.Item>
//             <Nav.Item>
//                 <Nav.Link eventKey="link-2">Link</Nav.Link>
//             </Nav.Item>
//             <Nav.Item>
//                 <Nav.Link eventKey="disabled" disabled>
//                 Disabled
//                 </Nav.Link>
//             </Nav.Item>
//             </Nav>
//         </>
//     );
// }

export default Sidebar