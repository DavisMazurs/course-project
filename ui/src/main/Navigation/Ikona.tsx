import React from "react";
import { IconType } from "react-icons";

interface IkonaProps {
    Icon: IconType;
}

const Ikona: React.FC<IkonaProps> = ({
    Icon
}) => {
    return (
        <Icon />
    );
};

export default Ikona;