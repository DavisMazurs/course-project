import React from "react";
import Footer from "../Footer/Footer";
import Top from "../Top/Top";
import ForkMe from "../ForkMe/ForkMe";
import { UserContext } from "../../contexts/UserContext/UserContext";
import Loader from "../Loader/Loader";
import Routes from "../Routes/Routes";
import useLoginOnApiKey from "./useLoginOnApiKey";
import useLocalStoragedApiKey from "./useLocalStoragedApiKey";
import { Col, Container, Row } from "react-bootstrap";
import Sidebar from "../Navigation/Sidebar";
import Navbar from "../Navigation/Navbar";

const Main: React.FC = () => {
  const {
    state: { loggedIn },
  } = React.useContext(UserContext);

  useLocalStoragedApiKey();
  useLoginOnApiKey();

  if (loggedIn === null) {
    return <Loader />;
  }

  return (
    <>
    {/* <Top /> */}
        <div className="wrapper">
          <Sidebar />
          <div className="main-panel">
            <Navbar />
            <Routes />
            <Footer />
          </div>
        </div>
    </>
  );
};

export default Main;
