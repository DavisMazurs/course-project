import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { Formik, Form as FormikForm } from "formik";
import { usePromise } from "react-use-promise-matcher";
import budgetService from "../services/BudgetService";
import FormikInput from "../parts/FormikInput/FormikInput";
import FeedbackButton from "../parts/FeedbackButton/FeedbackButton";
import { BiUserPlus } from "react-icons/bi";

const Category: React.FC = () => {

interface CategoryField {
  categoryName: string;
}

const [result, send, clear] = usePromise(({ categoryName }: CategoryField) =>
  budgetService.createNewCategory({ categoryName })
);

  return (
    <Container className="py-5">
      <Row>
        <Col md={9} lg={7} xl={6} className="mx-auto">
          <h3>Create new category</h3>
          <Formik<CategoryField>
            initialValues={{
              categoryName: 'hardcodeee'
            }}
            onSubmit={send}
          >
            <Form as={FormikForm}>
              <FormikInput name="category" label="Category name" />

              <FeedbackButton type="submit" label="Add" Icon={BiUserPlus} result={result} clear={clear} />
            </Form>
          </Formik>
        </Col>
      </Row>
    </Container>
  );
};

export default Category;
