import React from "react";
import { Container, Row, Table } from "react-bootstrap";

const ExpenseTracker: React.FC = () => {

    return (
        <Container>
            <Row>
                
            </Row>

            <Table bordered>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>01-02-2020</td>
                        <td>Example expense</td>
                        <td>Food</td>
                        <td>29.87 EUR</td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    )
}

export default ExpenseTracker;