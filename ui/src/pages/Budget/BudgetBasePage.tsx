import React, { useState } from "react";
import { Container, Row, Tab, Tabs } from "react-bootstrap";
import ExpenseTracker from "./ExpenseTracker/ExpenseTracker";

const BudgetBasePage: React.FC = () => {
    return (
        <Container>
            <Row>
                <h2>Budget App</h2>
            </Row>
            <Tabs defaultActiveKey="expenses">
                <Tab eventKey="expenses" title="Expense Tracker">
                    <ExpenseTracker />
                </Tab>
                <Tab eventKey="projects" title="Projects" disabled>

                </Tab>
                <Tab eventKey="shared" title="Shared Budget" disabled>

                </Tab>
            </Tabs>
        </Container>
    )
}

export default BudgetBasePage;