import axios, { AxiosRequestConfig } from "axios";

const endpoint = "api/v1/budget";

const createNewCategory = (body: {categoryName: string}) =>
    axios.post(`${endpoint}/create-category`, body);

export default {
    createNewCategory
};