package com.softwaremill.bootzooka.budget.expense

import cats.data.NonEmptyList
import com.softwaremill.bootzooka.infrastructure.Json._
import com.softwaremill.bootzooka.infrastructure.Doobie._
import com.softwaremill.bootzooka.http.Http
import com.softwaremill.bootzooka.security.{ApiKey, Auth}
import com.softwaremill.bootzooka.user.UserService
import com.softwaremill.bootzooka.util.ServerEndpoints
import monix.eval.Task

class ExpenseApi(http: Http, auth: Auth[ApiKey], expenseService: ExpenseService, userService: UserService, xa: Transactor[Task]) {
  import http._
  import ExpenseApi._

  private val EndpointPath = "budget/expense"

  private val getExpenseEndpoint = ???
//    secureEndpoint.get
//    .in(EndpointPath)
//    .out(jsonBody[GetExpense_OUT])
//    .serverLogic { authData =>
//      (for {
//        userId <-
//      } yield GetExpense_OUT())
//    }

  private val getExpensesEndpoint = secureEndpoint.get
    .in(EndpointPath / "list")
    .out(jsonBody[GetExpenses_OUT])
    .serverLogic { authData =>
      (for {
        userId <- auth(authData)
        user <- userService.findById(userId).transact(xa)
        expenses <- expenseService.getUserExpenses(user).transact(xa)
      } yield GetExpenses_OUT(expenses)).toOut
    }

  private val addExpenseEndpoint = secureEndpoint.post
    .in(EndpointPath / "add")
    .in(jsonBody[AddExpense_IN])
    .out(jsonBody[AddExpense_OUT])
    .serverLogic {
      case (authData, data) =>
        (for {
          userId <- auth(authData)
          _ <- expenseService.addExpense().transact(xa)
        } yield AddExpense_OUT()).toOut

    }

  private val updateExpenseEndpoint = ???

  private val deleteExpenseEndpoint = ???

  private val deleteExpensesEndpoint = ???

  val endpoints: ServerEndpoints =
    NonEmptyList
      .of(
        getExpenseEndpoint,
        getExpensesEndpoint,
        addExpenseEndpoint,
        updateExpenseEndpoint,
        deleteExpenseEndpoint,
        deleteExpensesEndpoint
      )
      .map(_.tag("expense"))
}

object ExpenseApi {

  case class GetExpense_OUT()

  case class GetExpenses_OUT(expenses: List[Expense])

  case class AddExpense_IN()
  case class AddExpense_OUT()

  case class UpdateExpense_IN()
  case class UpdateExpense_OUT()

  case class DeleteExpense_IN()
  case class DeleteExpense_OUT()

  case class DeleteExpenses_IN()
  case class DeleteExpenses_OUT()
}
