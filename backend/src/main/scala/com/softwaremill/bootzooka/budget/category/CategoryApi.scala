package com.softwaremill.bootzooka.budget.category

import cats.data.NonEmptyList
import com.softwaremill.bootzooka.infrastructure.Json._
import com.softwaremill.bootzooka.infrastructure.Doobie._
import com.softwaremill.bootzooka.http.Http
import com.softwaremill.bootzooka.util.ServerEndpoints
import monix.eval.Task

class CategoryApi(http: Http, budgetService: CategoryService, xa: Transactor[Task]) {
  import CategoryApi._
  import http._

  private val EndpointPath = "budget"

  private val createCategoryEndpoint = baseEndpoint.post
    .in(EndpointPath / "create-category")
    .in(jsonBody[CreateCategory_IN])
    .out(jsonBody[CreateCategory_OUT])
    .serverLogic { data =>
      (for {
        _ <- budgetService.createCategory(data.categoryName).transact(xa)
      } yield CreateCategory_OUT()).toOut
    }

  val endpoints: ServerEndpoints =
    NonEmptyList
      .of(createCategoryEndpoint)
      .map(_.tag("category"))
}

object CategoryApi {

  case class CreateCategory_IN(categoryName: String)
  case class CreateCategory_OUT()
}
