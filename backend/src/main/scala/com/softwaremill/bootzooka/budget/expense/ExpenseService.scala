package com.softwaremill.bootzooka.budget.expense

import com.softwaremill.bootzooka.security.ApiKeyService
import com.softwaremill.bootzooka.user.User
import com.softwaremill.bootzooka.util.IdGenerator
import com.typesafe.scalalogging.StrictLogging
import doobie.free.connection.ConnectionIO

class ExpenseService(expenseModel: ExpenseModel, apiKeyService: ApiKeyService, idGenerator: IdGenerator)  extends StrictLogging {

  def addExpense(): ConnectionIO[Unit] = ???

  def getUserExpenses(user: User): ConnectionIO[List[Expense]] = {
    expenseModel.listUserExpenses(user)
  }
}
