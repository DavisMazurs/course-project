package com.softwaremill.bootzooka.budget.category

import com.softwaremill.bootzooka.user.User
import com.softwaremill.bootzooka.util.IdGenerator
import com.softwaremill.bootzooka.infrastructure.Doobie._
import com.softwaremill.bootzooka.security.{ApiKey, ApiKeyService}
import com.softwaremill.bootzooka.util._
import com.softwaremill.tagging.@@
import com.typesafe.scalalogging.StrictLogging
import monix.execution.Scheduler.Implicits.global

import scala.concurrent.duration.Duration

class CategoryService(categoryModel: CategoryModel, apiKeyService: ApiKeyService, idGenerator: IdGenerator) extends StrictLogging {

  def createCategory(categoryName: String): ConnectionIO[Unit] = {
    for {
      id <- idGenerator.nextId().to[ConnectionIO]
      _ <- categoryModel.insert(id, "userId", categoryName)
//      apiKey <- apiKeyService.create(userId, Duration.Undefined)
    } yield ()
//    categoryModel.insert(categoryName)
  }

}
