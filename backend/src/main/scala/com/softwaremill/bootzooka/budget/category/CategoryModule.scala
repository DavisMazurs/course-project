package com.softwaremill.bootzooka.budget.category

import com.softwaremill.bootzooka.http.Http
import com.softwaremill.bootzooka.security.ApiKeyService
import com.softwaremill.bootzooka.util.BaseModule
import doobie.util.transactor.Transactor
import monix.eval.Task

trait CategoryModule extends BaseModule {
  lazy val categoryModel = new CategoryModel
  lazy val categoryApi = new CategoryApi(http, budgetService, xa)
  lazy val budgetService = new CategoryService(categoryModel, apiKeyService, idGenerator)

  def http: Http
  def apiKeyService: ApiKeyService
  def xa: Transactor[Task]
}
