package com.softwaremill.bootzooka.budget.expense

import com.softwaremill.bootzooka.email.{EmailScheduler, EmailTemplates}
import com.softwaremill.bootzooka.http.Http
import com.softwaremill.bootzooka.security.{ApiKey, ApiKeyService, Auth}
import com.softwaremill.bootzooka.user.{UserModel, UserService}
import com.softwaremill.bootzooka.util.BaseModule
import doobie.util.transactor.Transactor
import monix.eval.Task

trait ExpenseModule extends BaseModule {
  lazy val expenseModel = new ExpenseModel
  lazy val userModel = new UserModel
  lazy val expenseApi = new ExpenseApi(http, apiKeyAuth, expenseService, userService, xa)
  lazy val expenseService = new ExpenseService(expenseModel, apiKeyService, idGenerator)

  lazy val userService = new UserService(userModel, emailScheduler, emailTemplates, apiKeyService, idGenerator, clock, config.user)

  def http: Http
  def apiKeyAuth: Auth[ApiKey]
  def apiKeyService: ApiKeyService
  def xa: Transactor[Task]
  def emailScheduler: EmailScheduler
  def emailTemplates: EmailTemplates
}
