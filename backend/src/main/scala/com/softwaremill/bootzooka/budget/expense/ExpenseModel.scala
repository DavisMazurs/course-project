package com.softwaremill.bootzooka.budget.expense


import java.time.Instant

import cats.implicits._
import com.softwaremill.bootzooka.budget.category.Category
import com.softwaremill.bootzooka.infrastructure.Doobie._
import com.softwaremill.bootzooka.user.User
import com.softwaremill.bootzooka.util.{Id, LowerCased}
import com.softwaremill.tagging.@@
import org.http4s.client.oauth1.ProtocolParameter.Timestamp
import org.http4s.headers.Date

class ExpenseModel {

  def insert(expense: Expense): ConnectionIO[Unit] = {
    sql"""INSERT INTO budget_expenses (user_id, name, category, amount, attachment, creation_timestamp, purchase_timestamp)
         |VALUES (${expense.user_id}, ${expense.name}, ${expense.category}, ${expense.amount}, ${expense.attachment}, ${expense.creation_timestamp}, ${expense.purchase_timestamp})""".stripMargin.update.run.void
  }

  def listUserExpenses(user: User): ConnectionIO[List[Expense]] = {
    sql"""SELECT * FROM budget_expenses WHERE userid = ${user.id} ORDER BY purchase_timestamp DESC"""
  }.query[Expense].to[List]

}

case class Expense(
                    id: Id,
                    user_id: Id @@ User,
                    name: String,
                    category: Id @@ Category,
                    amount: Double,
                    attachment: String,
                    creation_timestamp: Timestamp,
                    purchase_timestamp: Timestamp
              )
