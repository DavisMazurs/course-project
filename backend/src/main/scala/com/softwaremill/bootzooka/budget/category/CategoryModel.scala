package com.softwaremill.bootzooka.budget.category

import cats.implicits._
import com.softwaremill.bootzooka.infrastructure.Doobie._
import com.softwaremill.bootzooka.user.User
import com.softwaremill.bootzooka.util.{Id, LowerCased}
import com.softwaremill.tagging.@@

class CategoryModel {

  def insert(id: Id, userId: String, name: String): ConnectionIO[Unit] = {
    sql"""INSERT INTO budget_categories (id, user_id, name)
         |VALUES (${id}, ${userId}, ${name})""".stripMargin.update.run.void
  }

}

case class Category(
                   id: Id,
                   user: Id @@ User,
                   name: String
                   )
